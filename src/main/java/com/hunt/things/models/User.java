package com.hunt.things.models;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection="users")
public class User implements UserDetails {
	private static final long serialVersionUID = 1L;

	@Id
    private String id;
    
    @Indexed(unique = true)
    private String email;
    
    @JsonIgnore
    private String password;
    @JsonIgnore
    private boolean enabled;
    
    public User() {
    }
    
    private User( Builder builder ) {
    	this.email = builder.email;
    	this.password = builder.password;
    	this.enabled = builder.enabled;
    	this.id = builder.id;
    	this.roles = builder.roles;
    }
    
    @DBRef
    @JsonIgnore
    private Set<Role> roles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
    
    @JsonProperty("roles")
    public Set<String> getPowers() {
    	return roles
    			.stream()
    			.map( Role::getRole )
    			.collect( Collectors.toSet() );
    }
    
	@Override
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles
				.stream()
				.map( role -> new SimpleGrantedAuthority(role.getRole())).collect(Collectors.toSet());
	}

	@Override
	public String getUsername() {
		return email;
	}
	
	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}
    
    public static class Builder {
        private String id;
        private String email;
        private String password;
        private Boolean enabled;
        private Set<Role> roles;
        
        public Builder id(String id) {
        	this.id = id;
        	return this;
        }
        
        public Builder email(String email ) {
        	this.email = email;
        	return this;
        }
        
        public Builder password(String password) {
        	this.password = password;
        	return this;
        }
        
        public Builder enabled(Boolean enabled) {
        	this.enabled = enabled;
        	return this;
        }
        
        public Builder roles(Set<Role> roles) {
        	this.roles = roles;
        	return this;
        }
        
        public User build() {
        	return new User(this);        	
        }
    }
}