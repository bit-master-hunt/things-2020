package com.hunt.things.controllers;

import java.security.Principal;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.exceptions.BadRequestException;
import com.hunt.things.models.Thing;
import com.hunt.things.services.ThingService;

@RestController
@RequestMapping( "/api/v1/things" )
public class ThingController {
	@Autowired
	private ThingService thingService;
	
	@RequestMapping( value="/name/{name}", method=RequestMethod.GET )
	public Collection<Thing> findByName( @PathVariable String name ) {
		return thingService.findByNameOrValue(name);
	}
	
	@RequestMapping( value="", method=RequestMethod.GET )
	public Iterable<Thing> getThings( Principal p ) {
		return thingService.findByOwner( p.getName() );
	}
	
	@RequestMapping( value="/{id}", method=RequestMethod.GET )
	public Thing getThing( @PathVariable String id ) {
		return thingService.findOne( id );
	}
	
	@RequestMapping( value="/{id}", method=RequestMethod.DELETE )
	public Thing deleteThing( @PathVariable String id ) {
		return thingService.deleteThing( id );
	}
	
	@RequestMapping( value="", method=RequestMethod.POST ) 
	public Thing createThing( @RequestBody Thing thing ) {
		return thingService.createThing( thing );
	}
	
	@RequestMapping( value="/{id}", method=RequestMethod.PUT )
	public Thing updateThing( @RequestBody Thing thing ) throws BadRequestException {
		return thingService.updateThing( thing );
	}
}
