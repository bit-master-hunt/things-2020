package com.hunt.things.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hunt.things.models.Photo;
import com.hunt.things.models.User;
import com.hunt.things.services.UserService;

@RestController
@RequestMapping("api/v1/profile")
public class PhotoController {
	@Autowired
	private UserService userService;
			
	// The file-type input element is named "photo"
	@RequestMapping(value="/photo", method=RequestMethod.PUT)
	public User setPhoto( 
			@RequestParam(name="photo", required=true) MultipartFile file, 
			Principal principal, 
			HttpServletResponse response ) {
		try {
			return userService.setPhoto( principal.getName(), file);
		} catch( Exception e ) {
	        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	        return null;
		}
	}
	
	@RequestMapping(value="/photo", method=RequestMethod.GET)
    public ResponseEntity<byte[]> getPhoto(Principal p, HttpServletResponse response) {
        Photo photo = userService.getPhoto( p.getName() );
        return ResponseEntity
        	.ok()
        	.contentType(MediaType.valueOf("image/" + photo.getType()))
        	.body( photo.getImage().getData());
    }
	
}
