package com.hunt.things.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.models.User;
import com.hunt.things.services.UserService;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public List<User> getUsers() {
		return userService.getUsers();
	}
	
	@RequestMapping(value="/users/{uid}", method=RequestMethod.GET)
	public User getUser( @PathVariable String uid ) {
		return userService.getUser( uid );
	}
	
	@RequestMapping(value="/users/{uid}/enabled", method=RequestMethod.PUT)
	public void setEnabled( @PathVariable String uid, @RequestBody(required=true) Boolean enabled, HttpServletResponse response ) {		
		if( userService.enableUser( uid, enabled) ) {
	        response.setStatus(HttpServletResponse.SC_OK);
		} else {
	        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	

	
	
}
