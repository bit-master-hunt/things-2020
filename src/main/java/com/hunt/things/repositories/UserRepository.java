package com.hunt.things.repositories;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.hunt.things.models.User;

public interface UserRepository extends MongoRepository<User, String> {
  Optional<User> findByEmail(String email);
  Boolean existsByUsername(String username);
  Boolean existsByEmail(String email);
}