package com.hunt.things.repositories;

import java.util.Collection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hunt.things.models.Thing;

@Repository
public interface ThingsRepository extends MongoRepository<Thing,String> {
	Collection<Thing> findByNameOrValue(String name, String value);
	Collection<Thing> findByOwner( String owner );
}
