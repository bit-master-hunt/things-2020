package com.hunt.things.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hunt.things.models.Photo;

public interface PhotoRepository extends MongoRepository<Photo, String> {
	public Photo findByOwner( String owner );
}