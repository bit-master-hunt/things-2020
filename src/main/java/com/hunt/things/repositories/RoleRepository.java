package com.hunt.things.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hunt.things.models.Role;

@Repository
public interface RoleRepository extends MongoRepository<Role,String> {
}
