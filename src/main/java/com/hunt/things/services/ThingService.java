package com.hunt.things.services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.things.exceptions.BadRequestException;
import com.hunt.things.models.Thing;
import com.hunt.things.repositories.ThingsRepository;

@Service
public class ThingService {
	@Autowired
	private ThingsRepository thingRepository;
	
	public Iterable<Thing> findAll() {
		return thingRepository.findAll();
	}
	
	public Collection<Thing> findByNameOrValue(String name) {
		return thingRepository.findByNameOrValue( name, name );
	}

	public Thing findOne(String id) {
		return thingRepository.findById( id ).orElse( null );
	}

	public Thing deleteThing(String id) {
		Thing oldThing = findOne( id );
		thingRepository.deleteById( id );
		return oldThing;
	}

	public Thing createThing( Thing thing ) {
		return thingRepository.save(thing);
	}
	
	public Thing updateThing(Thing thing) throws BadRequestException {
		return thingRepository.save( thing );
	}
	
	public Collection<Thing> findByOwner( String owner ) {
		return thingRepository.findByOwner(owner);
	}
}
