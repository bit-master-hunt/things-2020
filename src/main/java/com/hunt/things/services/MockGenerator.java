package com.hunt.things.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hunt.things.models.Role;
import com.hunt.things.models.Thing;
import com.hunt.things.models.User;
import com.hunt.things.repositories.RoleRepository;
import com.hunt.things.repositories.ThingsRepository;
import com.hunt.things.repositories.UserRepository;

@Service
public class MockGenerator {
	@Autowired
	private ThingsRepository thingsRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	PasswordEncoder encoder;

	private static String randomString() {
		final String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		final int N = new Random().nextInt(10) + 1;
        StringBuilder sb = new StringBuilder( N );

        new Random().ints(1, chars.length())
        	.limit(N)
        	.forEach( n -> {
        		sb.append( chars.charAt( n) );
        	});
        
        return sb.toString();
    }
	
	private List<Role> mockRoles() {
		return Arrays.asList( new Role.Builder().role("ROLE_USER").build(),
							  new Role.Builder().role("ROLE_ADMIN").build() )
				.stream()
				.map( role -> {
					return roleRepository.save( role );
				})
				.collect( Collectors.toList() );		
	}
	
	private List<User> mockUsers( List<Role> roles ) {
		Set<Role> s1 = new HashSet<Role>( roles.subList(0,  1) );
		Set<Role> s2 = new HashSet<Role>( roles.subList(1, 2) );
		Set<Role> s3 = new HashSet<Role>( roles );
		
		String encrypted123 = this.encoder.encode("123");
		
		return Arrays.asList(
				new User.Builder().email("user@uwlax.edu").password(encrypted123).enabled(true).roles(s1).build(),
				new User.Builder().email("admin@uwlax.edu").password(encrypted123).enabled(true).roles(s2).build(),
				new User.Builder().email("useradmin@uwlax.edu").password(encrypted123).enabled(true).roles(s3).build())
		.stream()
		.map( user -> {
			return userRepository.save(user);
		}).collect( Collectors.toList());
	}
	
	private void mockThings(List<User> users) {
		users.stream()
			.forEach( user -> {
				int n = new Random().nextInt(20) + 5;
				IntStream.range(1, n).forEach( i -> {
					Thing t = new Thing.Builder()
								.name(randomString())
								.value(randomString())
								.owner(user.getEmail())
								.build();
					System.out.println("saving: " + t );
					thingsRepository.save(t);
				});
			});
	}
	
	@PostConstruct
	private void mockData() {
		List<Role> roles = this.roleRepository.findAll();
		if( roles.isEmpty() ) {
			mockThings( mockUsers( mockRoles() ) );
		}
	}
	
}
