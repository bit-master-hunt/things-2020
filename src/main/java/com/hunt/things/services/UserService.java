package com.hunt.things.services;

import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hunt.things.models.Photo;
import com.hunt.things.models.User;
import com.hunt.things.repositories.PhotoRepository;
import com.hunt.things.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PhotoRepository photoRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository
				.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));		
	}
	
	public List<User> getUsers() {
		return userRepository.findAll();
	}

	public User getUser(String id ) {
		return userRepository.findById( id ).orElse( null );
	}
	
	public boolean enableUser( String id, boolean enabled ) {
		User user = userRepository.findById( id ).orElse( null );
		if( user != null ) {
			user.setEnabled( enabled );
			userRepository.save( user );
			return true;
		} else {
			return false;
		}
	}
	
	private static String imageType( String filename ) throws IllegalArgumentException {
		String extension = FilenameUtils.getExtension( filename ).toUpperCase();
		
		switch(extension) {
		case "PNG": 
		case "JPEG":
		case "JPG":
		case "GIF":	return extension;
		}
		
		throw new IllegalArgumentException();
	}

    public Photo getPhoto(String email) {
		User user = userRepository.findByEmail( email ).orElseThrow( IllegalArgumentException::new );		
        return photoRepository.findByOwner( user.getId() );
    }
    
	public User setPhoto(String email, MultipartFile file) throws IOException, IllegalArgumentException {
		User user = userRepository.findByEmail( email ).orElseThrow( IllegalArgumentException::new );
		String type = imageType( file.getOriginalFilename());	
        Photo photo = new Photo.Builder()
        		.owner(user.getId())
        		.type(type)
        		.image(new Binary(BsonBinarySubType.BINARY, file.getBytes()))
        		.build();
        
        Photo oldPhoto = getPhoto( email );
        if( oldPhoto != null ) {
        	photoRepository.delete(oldPhoto);
        }
        
        photo = photoRepository.save(photo);        
        return userRepository.save(user);
	}	

}