package com.hunt.things.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.hunt.things.security.AccessDeniedHandlerImpl;
import com.hunt.things.security.LoginFailureHandler;
import com.hunt.things.security.LoginSuccessHandler;
import com.hunt.things.security.LogoutSuccessHandlerImpl;
import com.hunt.things.services.UserService;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserService userDetailsService;

	@Autowired
	LoginSuccessHandler loginSuccessHandler;
	
	@Autowired
	LoginFailureHandler loginFailureHandler;

	@Autowired
	private LogoutSuccessHandlerImpl logoutSuccessHandler;

	@Autowired
	private AccessDeniedHandlerImpl accessDeniedHandler;
	
	@Bean
	public UserDetailsService userDetailsService() {
		return this.userDetailsService;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Autowired
	public void configAuthBuilder(AuthenticationManagerBuilder authBuilder) throws Exception {
		authBuilder.userDetailsService( userDetailsService );
	}
	
	private CsrfTokenRepository csrfTokenRepository() {
		CookieCsrfTokenRepository repository = new CookieCsrfTokenRepository();
		repository.setCookieHttpOnly(false);
		return repository;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors()
				.and()
			.csrf()
				.disable()
				//.csrfTokenRepository(csrfTokenRepository())				
				//.requireCsrfProtectionMatcher(new AntPathRequestMatcher("/api/**"))
				//.and()
			.formLogin()
				.successHandler( loginSuccessHandler )
				.failureHandler( loginFailureHandler )
				.loginProcessingUrl("/login")
				//.usernameParameter("username") uncomment if you want a different name
				//.passwordParameter("password") uncomment if you want a different name
				.and()
			.logout()
				.invalidateHttpSession(true)
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessHandler(logoutSuccessHandler)
				.and()
			.authorizeRequests()
				.antMatchers("/api/admin/**").hasRole("ADMIN")
			.anyRequest()
				.authenticated()
				.and()
			.exceptionHandling()
				.accessDeniedHandler(accessDeniedHandler);
	}
}